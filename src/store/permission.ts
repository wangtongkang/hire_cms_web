import { defineStore } from "pinia";
export const usePermissionStore = defineStore('permissionStore', {
    state: () => {
        return {
            roles: [],
            permissions: [],
            btnPermissions: [],
        }
    },
    persist: {
        enabled: true,
        strategies: [{
            key: 'roles',
            storage: window.sessionStorage,
            paths: ['roles']
        },
        {
            key: 'permissions',
            storage: window.sessionStorage,
            paths: ['permissions']
        },
        ]
    },
    getters: {},
    actions: {
        setRoles(roles: any) {
            this.roles = roles;
        },
        setPermissions(permissions: any) {
            this.permissions = permissions;
        },
        setBtnPermissions(btnPermissions: any) {
            this.btnPermissions = btnPermissions;
        }
    }
});

