import { createApp, Directive } from 'vue'
// import './style.css'
import App from './App.vue'
import router from '@/router';
import { createPinia } from 'pinia';
import piniaPersist from 'pinia-plugin-persist';
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate';
// 样式文件加载
import antd from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';
// 导入组件库
import * as antIcons from '@ant-design/icons-vue'
// 加载自定义指令
import * as directives from '@/directives'

const pinia = createPinia();
pinia.use(piniaPluginPersistedstate);
const app = createApp(App);

console.log('directives', directives);//打印自定义指令名
Object.keys(directives).forEach(key => {
    console.log('key', key);
    app.directive(key, (directives as { [key: string]: Directive })[key])
});
let icons: any = antIcons;
// 注册组件
Object.keys(icons).forEach((key: string) => {
    app.component(key, icons[key]);
})
// 添加到全局
app.config.globalProperties.$antIcons = icons;
app.config.globalProperties.$abc = 123;

app.use(router);
app.use(antd);
app.use(pinia);

app.mount('#app');

