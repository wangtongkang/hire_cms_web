import { get, post } from './request'

export default {
    alogin: function (code: any, xid: any, appToken: any) {
        console.log("import.meta.env.PROD", import.meta.env.PROD)
        // if (import.meta.env.PROD) {
        return get(`/api/auth/admin?code=${code}&xid=${xid}&appToken=${appToken}`, undefined, false);
        // } else {
        //   return get(`/api/auth/authTemp?username=${code}`, undefined, false);
        // }
    },
    setUser: function (user: any) {
        localStorage.setItem("user", JSON.stringify(user));
    },
    getUser: function () {
        const str = localStorage.getItem('user');
        if (!str) return null;
        const user = JSON.parse(str);

        return user;
    },
    getToken: function () {
        const user = this.getUser();
        if (user) return user.token;

        return null;
    },
    removeToken: function () {
        localStorage.removeItem('user');
    }
}