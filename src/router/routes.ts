export const routes = [
    {
        path: '',
        redirect: '/admin/home'
    },
    {
        path: '/login',
        name: 'login',
        meta: {
            noAuth: true,
        },
        component: () => import('@/views/user/Login.vue')
    },
    {
        path: '/change-pwd',
        name: 'change-pwd',
        meta: {
            noAuth: true,
        },
        component: () => import('@/views/user/ChangePassword.vue')
    },
    {
        path: '/forget-pwd',
        name: 'forget-pwd',
        meta: {
            noAuth: true,
        },
        component: () => import('@/views/user/ForgetPassword.vue')
    },
    {
        path: '/errPage',
        name: 'ErrPage',
        meta: {
            noAuth: true,
        },
        component: () => import('@/views/user/ErrPage.vue')
    },
    {
        path: '/',
        component: () => import('@/layout/adminLayout.vue'),
        children: [    
            {
                path: 'admin',
                name: 'admin',
                component: () => import('@/views/index.vue'),
                children: [
                    {
                        // 首页
                        path: 'home',
                        name: 'home',
                        meta: {
                            hideInMenu: false,
                            moduleIcon: 'AppstoreOutlined',
                            title: '首页', menuKey: '0',
                            menu: false,
                            noAuth: false,
                        },
                        component: () => import('@/views/Home.vue'),
                    },
                    // 项目管理路由
                    {
                        path: 'project-manage',
                        name: 'project-manage',
                        meta: {
                            hideInMenu: false,
                            moduleIcon: 'AppstoreOutlined',
                            title: '项目管理', menuKey: '1',
                            menu: false,
                            noAuth: false,
                        },
                        component: () => import('@/views/project-manage/index.vue'),
                        children: [
                            // 项目展示
                            {
                                path: 'project-show',
                                name: 'project-show',
                                children: [
                                    {
                                        path: '',
                                        name: '',
                                        meta: { noAuth: false, pointName: '查看项目展示列表',title: '项目展示', menuKey: '1.1.1', menu: true, module: '项目管理', icon: 'DesktopOutlined' },
                                        component: () => import('@/views/project-manage/project-show/index.vue')
                                    },
                                    {
                                        path: 'detail/:id',
                                        name: 'detail',
                                        props: true,
                                        meta: { noAuth: false, pointName: '查看项目展示详情',title: '项目详情', menuKey: '1.1.2', menu: true, module: '项目管理', icon: 'DownloadOutlined' },
                                        component: () => import('@/views/project-manage/project-show/Detail.vue')
                                    },
                                ]
                            },

                            // 导入记录
                            {
                                path: 'import-record',
                                name: 'import-record',

                                meta: { noAuth: false, pointName: '查看导入记录列表',title: '导入记录', menuKey: '1.2', menu: true, module: '项目管理', icon: 'FolderAddOutlined' },
                                component: () => import('@/views/project-manage/ImportRecord.vue')
                            },
                            // 下载中心
                            {
                                path: 'download-center',
                                name: 'download-center',

                                meta: { noAuth: false, pointName: '查看下载中心列表',title: '下载中心', menuKey: '1.3', menu: true, module: '项目管理', icon: 'DownloadOutlined' },
                                component: () => import('@/views/project-manage/DownloadCenter.vue')
                            },
                            // 项目上线
                            {
                                path: 'project-up',
                                name: 'project-up',

                                meta: { noAuth: false, pointName: '查看项目上线列表',title: '项目上线', menuKey: '1.4', menu: true, module: '项目管理', icon: 'NodeExpandOutlined' },
                                component: () => import('@/views/project-manage/ProjectUp.vue')
                            },
                            // 项目推广
                            {
                                path: 'project-push',
                                name: 'project-push',
                                meta: { noAuth: false,pointName: '查看项目推广列表', title: '项目推广', menuKey: '1.5', menu: true, module: '项目管理', icon: 'FlagOutlined' },
                                component: () => import('@/views/project-manage/ProjectPush.vue')
                            },
                        ]
                    },
                    // 系统设置
                    {
                        path: 'system-setting',
                        name: 'system-setting',
                        meta: {
                            hideInMenu: true,
                            moduleIcon: 'SettingOutlined',
                            title: '系统设置', menuKey: '2',
                            menu: false,
                            noAuth: false,
                        },
                        // component: () => import('@/views/system-setting/index.vue'),
                        children: [
                            // 项目类型
                            {

                                path: 'project-type',
                                name: 'project-type',
                                meta: { noAuth: false, pointName: '查看项目类型列表',title: '项目类型', menuKey: '2.1', menu: true, module: '系统设置', icon: 'ApartmentOutlined' },
                                component: () => import('@/views/system-setting/ProjectType.vue')
                            },
                            // 评分平台
                            {
                                path: 'score-platform',
                                name: 'score-platform',
                                meta: { noAuth: false, pointName: '查看评分平台列表',title: '评分平台', menuKey: '2.2', menu: true, module: '系统设置', icon: 'DesktopOutlined' },
                                component: () => import('@/views/system-setting/ScorePlatform.vue')
                            },
                        ]
                    },
                    // 用户管理
                    {
                        path: 'user-manage',
                        name: 'user-manage',
                        meta: {
                            hideInMenu: true,
                            moduleIcon: 'UserOutlined',
                            title: '用户管理', menuKey: '3',
                            menu: false,
                            noAuth: false,
                        },
                        // component: () => import('@/views/user-manage/index.vue'),
                        children: [
                            // 用户管理
                            {
                                path: 'user-manage',
                                name: 'user-manage',
                                meta: { noAuth: false, pointName: '查看用户管理列表',title: '用户管理', menuKey: '3.1', menu: true, module: '用户管理', icon: 'UserOutlined' },
                                component: () => import('@/views/user-manage/UserManage.vue')
                            },
                            // 角色管理
                            {
                                path: 'role-manage',
                                name: 'role-manage',
                                meta: { noAuth: false, pointName: '查看角色管理列表',title: '角色管理', menuKey: '3.2', menu: true, module: '用户管理', icon: 'UserSwitchOutlined' },
                                component: () => import('@/views/user-manage/RoleManage.vue')
                            },
                        ]
                    },
                    // 用户日志
                    {
                        path: 'user-log',
                        name: 'user-log',
                        meta: {
                            hideInMenu: true,
                            noAuth: false,
                            moduleIcon: 'UserOutlined',
                        },
                        // component: () => import('@/views/user-log/index.vue'),
                        children: [
                            // 用户日志
                            {
                                path: 'list',
                                meta: { noAuth: false, pointName: '查看用户日志列表',title: '用户日志', menuKey: '4', menu: true, module: '用户日志', icon: 'FileOutlined' },
                                component: () => import('@/views/user-log/UserLog.vue')
                            }
                        ]
                    },
                    // // 官网维护
                    // {
                    //     path: 'web-vindicate',
                    //     name: 'web-vindicate',
                    //     meta: {
                    //         hideInMenu: true,
                    //     },
                    //     component: () => import('@/views/web-vindicate/index.vue'),
                    //     children: [
                    //         // 官网链接
                    //         {
                    //             path: '',
                    //             component: () => import('@/views/web-vindicate/WebLink.vue')
                    //         },
                    //     ]
                    // },
                ]
            }
        ]
    }
]