import { Directive } from "vue";

export const permission: Directive = {
    mounted(el, binding) {
        // 获取用户使用自定义指令绑定的内容
        const { value } = binding;
        // 获取权限按钮
        const permissionBtn: any = localStorage.getItem('permissionStore');
        console.log('权限Btn', permissionBtn);
        // 判断用户使用自定义指令，是否使用正确了
        if (value && value instanceof Array && value.length > 0) {
            const permissionFunc = value;
            // 判断传递进来的用户权限，用户是否拥有
            const hasPermission = permissionBtn?.some((role: any) => {
                return permissionFunc.includes(role);
            });
            // 没有权限，隐藏按钮
            if (!hasPermission) {
                el.style.display = 'none'
            }
        }else{
            // throw new Error('need roles! Like v-permission="[\'admin\',\'editor\']"'); 
        }
    }
}