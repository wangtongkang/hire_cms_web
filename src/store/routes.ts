import { defineStore } from "pinia";
import router from "@/router";

export const useRouteStore = defineStore({
    id: 'routeStore',
    state: () => ({
        tagsRouter: [] as any[],
    }),
    getters: {
        getTagsRoute(state) {
            // 获取标签路由信息
            return state.tagsRouter;
        }
    },
    actions: {
        setRouterTagActive(routeItem: { name: any; }) {
            // 设置当前的tab页为活动页
            for (let i = 0; i < this.tagsRouter.length; i++) {
                let item = this.tagsRouter[i];
                this.tagsRouter[i].isActivate = false;
                if (this.tagsRouter[i].name === routeItem.name) {
                    this.tagsRouter[i].isActivate = true;
                }
            }
            console.log('routeStore当前存有的标签路由', this.tagsRouter);
        },
        // 添加当前路由
        addRoute(routeItem: { name: any; path: any; meta: any; }) {
            // debugger;
            let flag = false;
            if (this.tagsRouter.length > 0) {
                for (let index = 0; index < this.tagsRouter.length; index++) {
                    if (this.tagsRouter[index].name === routeItem.name) {
                        flag = true;
                        break;
                    }
                }
                if (!flag) {
                    this.tagsRouter.push({
                        path: routeItem.path,
                        name: routeItem.name,
                        meta: routeItem.meta,
                    })
                }
            } else {
                this.tagsRouter.push({
                    path: routeItem.path,
                    name: routeItem.name,
                    meta: routeItem.meta,
                })
            }


            console.log('添加后的路由标签组', this.tagsRouter);

        },
        // 删除路由
        delRoute(params: { meta: { menuKey: any; }; }) {
            console.log('删除路由被调用');
            if (this.tagsRouter.length != 0) {
                let itemIndex = 0;
                for (let index = 0; index < this.tagsRouter.length; index++) {
                    if (this.tagsRouter[index].meta.menuKey == params.meta.menuKey) {
                        itemIndex = index;
                        this.tagsRouter.splice(itemIndex, 1);
                        break;
                    }
                }
                // 删除后的操作
            }
        }
    }
})

export default useRouteStore
