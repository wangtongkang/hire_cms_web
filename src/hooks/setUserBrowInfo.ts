import { post } from "@/service/request";
import { userInfo } from "@/store/user";
import dayjs from 'dayjs';

const token = userInfo().info;
// 统计用户浏览信息
function setUserBrowInfo(content: any) {
    if (token) {
        let currentTime = dayjs();
        let currentTimeFormat = currentTime.format('YYYY-MM-DD hh:mm:ss');
        let browParam = {
            content: content,
            createDate: currentTimeFormat,
            creatorId: token.id,
            creator: token.name,
        }
        post(`/operator/save`, browParam).then((res: any) => {
            if (res.errCode == 0) {
                // console.log('埋点成功');
            } else {
                // console.log('===暂不处理===', res.msg);
            }
        });
    }
}

export { setUserBrowInfo }

