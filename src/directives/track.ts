import {Directive} from "vue";
import {post} from "@/service/request";

export const track: Directive = {
    mounted(el, binding) {
        //这里参数是根据自己业务可以自己定义
        const {value} = binding;
        // let params = {
        //     content: binding.value,
        //     // service: "xxx",
        // };
        //如果是click类型，监听click事件
        el.addEventListener(
            "click",
            () => {
                if (value) {
                    post(`/operator/save`, {
                        content: value,
                    }).then((res: any) => {

                    });
                }
            },
            false
        );
    }
}