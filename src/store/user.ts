import { defineStore } from "pinia";
export const userInfo = defineStore('user', {
    state: () => {
        return {
            info: {
                id: Number,
                phone: String,
                name: String,
                menus: [],
            }
        }
    },
    persist: {
        enabled: true,
        strategies: [{
            key: 'user_info',
            storage: window.sessionStorage,
            paths: ['info']
        }]
    },
    getters: {},
    actions: {
        login(userInfo: any) {
            this.info = userInfo;
        }
    }
});

