// 获取cookies
const CookieTool = {
    getCookie: (name: string) => {
        var arr, reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");
        if (arr = document.cookie.match(reg)) {
            return (arr[2])
        } else {
            return null;
        }
    },

    // 设置cookie
    setCookie: (c_name: any, value: any, expiredays: any) => {
        var exdate = new Date();
        exdate.setDate(exdate.getDate() + expiredays);
        document.cookie = c_name + "=" + encodeURI(value) + ((expiredays == null) ? "" : ";expires=" + exdate.toUTCString());
    },
    // 删除cookie
    delCookie: (name: any) => {
        var exp = new Date();
        exp.setTime(exp.getTime() - 1);
        var cval = CookieTool.getCookie(name);
        if (cval != null) {
            document.cookie = name + "=" + cval + ";expires=" + exp.toUTCString();
        }
    }
}

export default CookieTool