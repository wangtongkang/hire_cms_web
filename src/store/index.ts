import { defineStore } from "pinia";

export const useStore = defineStore({
    id: 'user',
    state: () => ({
        count: 12,
        name: '张三',
        id: 15
    }),
    getters: {
        countSum(state) {
            return state.count + 10;
        }
    },
    actions: {
        logout() {
            this.$patch({
                name: 'lisi'
            })
        }
    }
})

export default useStore
